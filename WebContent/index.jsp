<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>网管监控平台</title>

<link href="<%=request.getContextPath() %>/dwz/themes/default/style.css" rel="stylesheet" type="text/css" media="screen" />
<link href="<%=request.getContextPath() %>/dwz/themes/css/core.css" rel="stylesheet" type="text/css" media="screen" />
<link href="<%=request.getContextPath() %>/dwz/themes/css/print.css" rel="stylesheet" type="text/css" media="print" />
<link href="<%=request.getContextPath() %>/dwz/uploadify/css/uploadify.css" rel="stylesheet" type="text/css" media="screen" />
<!--[if IE]>
<link href="themes/css/ieHack.css" rel="stylesheet" type="text/css" media="screen"/>
<![endif]-->

<!--[if lte IE 9]>
<script src="js/speedup.js" type="text/javascript"></script>
<![endif]-->

<script src="<%=request.getContextPath() %>/dwz/js/jquery-1.7.2.js" type="text/javascript"></script>
<script src="<%=request.getContextPath() %>/dwz/js/jquery.cookie.js" type="text/javascript"></script>
<script src="<%=request.getContextPath() %>/dwz/js/jquery.validate.js" type="text/javascript"></script>
<script src="<%=request.getContextPath() %>/dwz/js/jquery.bgiframe.js" type="text/javascript"></script>
<script src="<%=request.getContextPath() %>/dwz/xheditor/xheditor-1.2.1.min.js" type="text/javascript"></script>
<script src="<%=request.getContextPath() %>/dwz/xheditor/xheditor_lang/zh-cn.js" type="text/javascript"></script>
<script src="<%=request.getContextPath() %>/dwz/uploadify/scripts/jquery.uploadify.js" type="text/javascript"></script>

<script src="<%=request.getContextPath() %>/dwz/js/dwz.core.js" type="text/javascript"></script>
<script src="<%=request.getContextPath() %>/dwz/js/dwz.util.date.js" type="text/javascript"></script>
<script src="<%=request.getContextPath() %>/dwz/js/dwz.validate.method.js" type="text/javascript"></script>
<script src="<%=request.getContextPath() %>/dwz/js/dwz.barDrag.js" type="text/javascript"></script>
<script src="<%=request.getContextPath() %>/dwz/js/dwz.drag.js" type="text/javascript"></script>
<script src="<%=request.getContextPath() %>/dwz/js/dwz.tree.js" type="text/javascript"></script>
<script src="<%=request.getContextPath() %>/dwz/js/dwz.accordion.js" type="text/javascript"></script>
<script src="<%=request.getContextPath() %>/dwz/js/dwz.ui.js" type="text/javascript"></script>
<script src="<%=request.getContextPath() %>/dwz/js/dwz.theme.js" type="text/javascript"></script>
<script src="<%=request.getContextPath() %>/dwz/js/dwz.switchEnv.js" type="text/javascript"></script>
<script src="<%=request.getContextPath() %>/dwz/js/dwz.alertMsg.js" type="text/javascript"></script>
<script src="<%=request.getContextPath() %>/dwz/js/dwz.contextmenu.js" type="text/javascript"></script>
<script src="<%=request.getContextPath() %>/dwz/js/dwz.navTab.js" type="text/javascript"></script>
<script src="<%=request.getContextPath() %>/dwz/js/dwz.tab.js" type="text/javascript"></script>
<script src="<%=request.getContextPath() %>/dwz/js/dwz.resize.js" type="text/javascript"></script>
<script src="<%=request.getContextPath() %>/dwz/js/dwz.dialog.js" type="text/javascript"></script>
<script src="<%=request.getContextPath() %>/dwz/js/dwz.dialogDrag.js" type="text/javascript"></script>
<script src="<%=request.getContextPath() %>/dwz/js/dwz.sortDrag.js" type="text/javascript"></script>
<script src="<%=request.getContextPath() %>/dwz/js/dwz.cssTable.js" type="text/javascript"></script>
<script src="<%=request.getContextPath() %>/dwz/js/dwz.stable.js" type="text/javascript"></script>
<script src="<%=request.getContextPath() %>/dwz/js/dwz.taskBar.js" type="text/javascript"></script>
<script src="<%=request.getContextPath() %>/dwz/js/dwz.ajax.js" type="text/javascript"></script>
<script src="<%=request.getContextPath() %>/dwz/js/dwz.pagination.js" type="text/javascript"></script>
<script src="<%=request.getContextPath() %>/dwz/js/dwz.database.js" type="text/javascript"></script>
<script src="<%=request.getContextPath() %>/dwz/js/dwz.datepicker.js" type="text/javascript"></script>
<script src="<%=request.getContextPath() %>/dwz/js/dwz.effects.js" type="text/javascript"></script>
<script src="<%=request.getContextPath() %>/dwz/js/dwz.panel.js" type="text/javascript"></script>
<script src="<%=request.getContextPath() %>/dwz/js/dwz.checkbox.js" type="text/javascript"></script>
<script src="<%=request.getContextPath() %>/dwz/js/dwz.history.js" type="text/javascript"></script>
<script src="<%=request.getContextPath() %>/dwz/js/dwz.combox.js" type="text/javascript"></script>
<script src="<%=request.getContextPath() %>/dwz/js/dwz.print.js" type="text/javascript"></script>

<!-- 可以用dwz.min.js替换前面全部dwz.*.js (注意：替换是下面dwz.regional.zh.js还需要引入)
<script src="bin/dwz.min.js" type="text/javascript"></script>
-->
<script src="<%=request.getContextPath() %>/dwz/js/dwz.regional.zh.js" type="text/javascript"></script>

<script type="text/javascript">
	$(function() {
		DWZ.init("dwz.frag.xml", {
			//loginUrl : "login_dialog.html",
			//loginTitle : "登录", // 弹出登录对话框
			statusCode : {
				ok : 200,
				error : 300,
				timeout : 301
			}, //【可选】
			pageInfo : {
				pageNum : "pageNum",
				numPerPage : "numPerPage",
				orderField : "orderField",
				orderDirection : "orderDirection"
			}, //【可选】
			keys : {
				statusCode : "statusCode",
				message : "message"
			}, //【可选】
			ui : {
				hideMode : 'offsets'
			}, //【可选】hideMode:navTab组件切换的隐藏方式，支持的值有’display’，’offsets’负数偏移位置的值，默认值为’display’
			debug : true, // 调试模式 【true|false】
			callback : function() {
				initEnv();
				$("#themeList").theme({themeBase:"dwz/themes"}); // themeBase 相对于index页面的主题base路径
// 				$("#themeList li div.selected").click();
			}
		});
	});
</script>
<style type="text/css">
	.commonDiv table {border-collapse:separate; border-spacing:5px;width:100%;height: 100%}
	.commonDiv table tbody tr td {padding: 3px;}
	.commonDiv table div{border: 1px solid #b8d0d6;}
	
	a:HOVER {text-decoration: none;}
</style>
</head>
<body scroll="no">
	<div id="layout">
		<div id="header">
			<div class="headerNav">
				<a class="logo" href="http://j-ui.com">标志</a>
				<ul class="nav">
					<li><a href="login.html">管理员</a></li>
					<li><a href="login.html">退出</a></li>
				</ul>
				<ul class="themeList" id="themeList">
					<li theme="default"><div class="selected">蓝色</div></li>
					<li theme="green"><div>绿色</div></li>
					<li theme="red"><div>红色</div></li>
					<li theme="purple"><div>紫色</div></li>
					<li theme="silver"><div>银色</div></li>
					<li theme="azure"><div>天蓝</div></li>
				</ul>
			</div>
			<!-- navMenu -->
		</div>

		<div id="leftside">
			<div id="sidebar_s">
				<div class="collapse">
					<div class="toggleCollapse"><div></div></div>
				</div>
			</div>
			<div id="sidebar">
				<div class="toggleCollapse"><h2></h2><div>收缩</div></div>

				<div class="accordion" fillSpace="sidebar">
					<div class="accordionHeader">
						<h2><span>Folder</span>菜单列表</h2>
					</div>
					<div class="accordionContent">
						<ul class="tree treeFolder expand">
							<li><a >系统监控</a>
								<ul>
									<li>
										<a href="cpu.do" target="navTab" rel="cpu_tab" title="CPU监控"">CPU</a>
										<a href="mem.do" target="navTab" rel="mem_tab" title="内存监控">内存</a>
										<a href="disk.do" target="navTab" rel="disk_tab" title="磁盘监控">磁盘</a>
									</li>
								</ul>
							</li>
							<li><a>业务监控</a>
								<ul>
									<li>
										<a href="###" target="navTab" external="true">门户</a>
										<a href="###" target="navTab" external="true">APP</a>
										<a href="###" target="navTab" external="true">ACS</a>
										<a href="###" target="navTab" external="true">NBI</a>
										<a href="###" target="navTab" external="true">报表</a>
										<a href="kpi.do" target="navTab" title="KPI监控" external="false">KPI</a>
									</li>
								</ul>
							</li>
							<li><a>服务器监控</a>
								<ul>
									<li>
										<a href="###" target="navTab" external="true">Tomcat</a>
										<a href="###" target="navTab" external="true">Mysql</a>
										<a href="###" target="navTab" external="true">Redis</a>
										<a href="###" target="navTab" external="true">Nginx</a>
										<a href="###" target="navTab" external="true">FTP</a>
									</li>
								</ul>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div id="container">
			<div id="navTab" class="tabsPage">
				<div class="tabsPageHeader">
					<div class="tabsPageHeaderContent"><!-- 显示左右控制时添加 class="tabsPageHeaderMargin" -->
						<ul class="navTab-tab">
							<li tabid="main" class="main">
								<a href="javascript:;"><span><span class="home_icon">我的主页</span></span></a>
							</li>
						</ul>
					</div>
					<div class="tabsLeft">left</div><!-- 禁用只需要添加一个样式 class="tabsLeft tabsLeftDisabled" -->
					<div class="tabsRight">right</div><!-- 禁用只需要添加一个样式 class="tabsRight tabsRightDisabled" -->
					<div class="tabsMore">more</div>
				</div>
				<ul class="tabsMoreList">
					<li><a href="javascript:;">我的主页</a></li>
				</ul>
				<div class="navTab-panel tabsPageContent layoutBox">
					<div class="page unitBox">
						<div class="pageFormContent" selector="h1" layoutH="80">
							<jsp:include page="index_main.jsp" />
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="footer">Copyright &copy; 2014 南京飞烽通信</div>
</body>
</html>