<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<style>
	.mainLabelDiv {border: 1px solid #b8d0d6;}
	.mainLabelDiv table {border-collapse:separate; border-spacing:5px;width:100%;height: 100%}
	.mainLabelDiv table td{text-align: left;padding: 3px;}
	.mainLabelDiv table td h1{margin-bottom: 5px;}
</style>
<div style="float: left;width: 100%;">
	<div style="width: 1%;float: left;">
		&nbsp;
	</div>
	<div style="width: 32%;float: left;text-align: center;">
		<div class="panel" defH="300">
			<h1 style="">服务器A</h1>
			<div class="mainLabelDiv">
				<table >
					<tr>
						<td >
							操作系统：
						</td>
						<td>Linux</td>
						<td>
							系统内核：
						</td>
						<td>x86_64</td>
					</tr>
					<tr>
						<td>
							系统描述：
						</td>
						<td>
							SuSE 11
						</td>
						<td>
							服务器IP：
						</td>
						<td>
							192.168.1.110
						</td>
					</tr>
					<tr>
						<td style="width: 25%;text-align:center;">
							<h1><a href="cpu.do" target="navTab" rel="cpu_tab" title="CPU监控">CPU</a></h1>
							<div style="height: 100px;">
								<iframe name="cpu_frame"  marginwidth="0" marginheight="0" width="100%" height="100%"
									src="chart/cpu-pie.jsp" scrolling="auto" frameborder=0></iframe>
							</div>
						</td>
						<td  style="text-align:center;width: 25%;">
							<h1><a href="mem.do" target="navTab" rel="mem_tab" title="内存监控">内存</a></h1>
							<div style="height: 100px;">
								<iframe name="mem_frame"  marginwidth="0" marginheight="0" width="100%" height="100%"
									src="chart/mem-pie.jsp" scrolling="auto" frameborder=0></iframe>
							</div>
						</td>
						<td  style="text-align:center;width: 25%;">
							<h1><a href="disk.do" target="navTab" rel="disk_tab" title="磁盘监控">磁盘</a></h1>
							<div style="height: 100px;">
								<iframe name="disk_frame"  marginwidth="0" marginheight="0" width="100%" height="100%"
									src="chart/disk-pie.jsp" scrolling="auto" frameborder=0></iframe>
							</div>
						</td>
						<td  style="text-align:center;width: 25%;">
							<h1><a href="##" title="数据库监控">数据库</a></h1>
							<div style="height: 100px;">
								<iframe name="disk_frame"  marginwidth="0" marginheight="0" width="100%" height="100%"
									src="chart/disk-pie.jsp" scrolling="auto" frameborder=0></iframe>
							</div>
						</td>
					</tr>
					<tr>
						<td>
							CPU：
							<font color="green">运行正常</font>
						</td>
						<td>
							内存：<font color="green">运行正常</font>
						</td>
						<td colspan="2">
							磁盘：<font color="green">运行正常</font>
						</td>
					</tr>
					<tr>
						<td>
							Redis：
							<a href="###"><font color="red">告警(1)</font></a>
						</td>
						<td>
							Mysql：
							<a href="###"><font color="red">告警(2)</font></a>
						</td>
						<td colspan="2">
							Tomcat：
							<a href="###"><font color="red">错误(4)</font></a>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</div>
	<div style="width: 1%;float: left;">
		&nbsp;
	</div>
	<div style="width: 32%;float: left;text-align: center;">
		<div class="panel" defH="300">
			<h1>服务器A</h1>
			<div class="mainLabelDiv">
				<table >
					<tr>
						<td >
							操作系统：
						</td>
						<td>Linux</td>
						<td>
							系统内核：
						</td>
						<td>x86_64</td>
					</tr>
					<tr>
						<td>
							系统描述：
						</td>
						<td>
							SuSE 11
						</td>
						<td>
							服务器IP：
						</td>
						<td>
							192.168.1.110
						</td>
					</tr>
					<tr>
						<td style="width: 25%;text-align:center;">
							<h1><a href="cpu.do" target="navTab" rel="cpu_tab" title="CPU监控">CPU</a></h1>
							<div style="height: 100px;">
								<iframe name="cpu_frame"  marginwidth="0" marginheight="0" width="100%" height="100%"
									src="chart/cpu-pie.jsp" scrolling="auto" frameborder=0></iframe>
							</div>
						</td>
						<td  style="text-align:center;width: 25%;">
							<h1><a href="mem.do" target="navTab" rel="mem_tab" title="内存监控">内存</a></h1>
							<div style="height: 100px;">
								<iframe name="mem_frame"  marginwidth="0" marginheight="0" width="100%" height="100%"
									src="chart/mem-pie.jsp" scrolling="auto" frameborder=0></iframe>
							</div>
						</td>
						<td  style="text-align:center;width: 25%;">
							<h1><a href="disk.do" target="navTab" rel="disk_tab" title="磁盘监控">磁盘</a></h1>
							<div style="height: 100px;">
								<iframe name="disk_frame"  marginwidth="0" marginheight="0" width="100%" height="100%"
									src="chart/disk-pie.jsp" scrolling="auto" frameborder=0></iframe>
							</div>
						</td>
						<td  style="text-align:center;width: 25%;">
							<h1><a href="##" target="navTab" rel="mysql_tab" title="数据库查看">数据库</a></h1>
							<div style="height: 100px;">
								<iframe name="disk_frame"  marginwidth="0" marginheight="0" width="100%" height="100%"
									src="chart/disk-pie.jsp" scrolling="auto" frameborder=0></iframe>
							</div>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</div>
	<div style="width: 1%;float: left;">
		&nbsp;
	</div>
	<div style="width: 32%;float: left;text-align: center;">
		<div class="panel" defH="300">
			<h1>服务器A</h1>
			<div class="mainLabelDiv">
				<table >
					<tr>
						<td >
							操作系统：
						</td>
						<td>Linux</td>
						<td>
							系统内核：
						</td>
						<td>x86_64</td>
					</tr>
					<tr>
						<td>
							系统描述：
						</td>
						<td>
							SuSE 11
						</td>
						<td>
							服务器IP：
						</td>
						<td>
							192.168.1.110
						</td>
					</tr>
					<tr>
						<td style="width: 25%;text-align:center;">
							<h1><a href="cpu.do" target="navTab" rel="cpu_tab" title="CPU监控">CPU</a></h1>
							<div style="height: 100px;">
								<iframe name="cpu_frame"  marginwidth="0" marginheight="0" width="100%" height="100%"
									src="chart/cpu-pie.jsp" scrolling="auto" frameborder=0></iframe>
							</div>
						</td>
						<td  style="text-align:center;width: 25%;">
							<h1><a href="mem.do" target="navTab" rel="mem_tab" title="内存监控">内存</a></h1>
							<div style="height: 100px;">
								<iframe name="mem_frame"  marginwidth="0" marginheight="0" width="100%" height="100%"
									src="chart/mem-pie.jsp" scrolling="auto" frameborder=0></iframe>
							</div>
						</td>
						<td  style="text-align:center;width: 25%;">
							<h1><a href="disk.do" target="navTab" rel="disk_tab" title="磁盘监控">磁盘</a></h1>
							<div style="height: 100px;">
								<iframe name="disk_frame"  marginwidth="0" marginheight="0" width="100%" height="100%"
									src="chart/disk-pie.jsp" scrolling="auto" frameborder=0></iframe>
							</div>
						</td>
						<td  style="text-align:center;width: 25%;">
							<h1><a href="##" target="navTab" rel="mysql_tab" title="数据库查看">数据库</a></h1>
							<div style="height: 100px;">
								<iframe name="disk_frame"  marginwidth="0" marginheight="0" width="100%" height="100%"
									src="chart/disk-pie.jsp" scrolling="auto" frameborder=0></iframe>
							</div>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</div>
	<div style="width: 1%;float: left;">
		&nbsp;
	</div>
</div>