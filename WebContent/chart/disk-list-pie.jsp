<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Pie Demo</title>
<script type="text/javascript" src="http://cdn.hcharts.cn/jquery/jquery-1.8.2.min.js"></script>
<script type="text/javascript" src="<%=request.getContextPath() %>/highcharts/js/highcharts.js"></script>
<script type="text/javascript">
$(function () {
    $(document).ready(function() {
    	$('#container').highcharts({
            chart: {
                renderTo: 'container',
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false
            },
            colors:[
                    '#492970',
                    '#f28f43', 
                    '#77a1e5', 
                    '#c42525', 
                    '#a6c96a'
                  ],
            title:{
				text:''
			},
            credits: {
           	  	enabled: false
           	},
            tooltip: {
        	    pointFormat: '<b>{point.percentage:.1f}%</b>',
        	    style: {
        	    	color: '#333333',
        	    	fontSize: '10px'
        	    }
            },
            plotOptions: {
	            pie: {
	                allowPointSelect: true,
	                cursor: 'pointer',
	                dataLabels: {
	                    enabled: true
	                },
	                showInLegend: true
	            }	
            },
            series: [{
                type: 'pie',
                name: '',
                data: [
                    ['已使用',   30.0],
                    ['剩余大小',   70.0]
                ]
            }]
        });
    });
});
</script>
</head>
<body>
	<div id="container" style="height:230px;margin: 0 auto"></div>
</body>
</html>