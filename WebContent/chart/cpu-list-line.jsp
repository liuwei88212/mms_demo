<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE HTML>
<%
	String id =request.getParameter("id");
	request.setAttribute("id", id);
%>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title></title>
		<script type="text/javascript" src="http://cdn.hcharts.cn/jquery/jquery-1.8.2.min.js"></script>
		<script src="<%=request.getContextPath() %>/highcharts/js/highcharts.js"></script>
		<script src="<%=request.getContextPath() %>/highcharts/js/modules/exporting.js"></script>
		<script type="text/javascript">
			$(function () {
			    $(document).ready(function() {
			        Highcharts.setOptions({
			            global: {
			                useUTC: false
						}
			        });
			        $('#container').highcharts({
						 chart: {
				                type: 'spline',
				                animation: Highcharts.svg, // don't animate in old IE
				                marginRight: 10,
				                events: {
				                    load: function() {
				                        // set up the updating of the chart each second
				                        var series = this.series[0];
				                        setInterval(function() {
				                            var x = (new Date()).getTime(), // current time
				                                y = Math.random();
				                            series.addPoint([x, y], true, true);
				                        }, 1000);
				                    }
				                }
				            },
				            credits: {
				           	  	enabled: false
				           	},
				            title: {
				                text: 'CPU-${id}'
				            },
				            xAxis: {
				                type: 'datetime',
				                tickPixelInterval: 120
				            },
				            yAxis: {
				            	title: {
				                    text: ''
				                },
				                plotLines: [{
				                    value: 0,
				                    width: 1,
				                    color: '#808080'
				                }],
				                max: 100
				            },
				            tooltip: {
				                formatter: function() {
				                        return '<b>'+ this.series.name +'</b><br/>'+Highcharts.numberFormat(this.y,0)+'%';
				                }
				            },
				            legend: {
				                enabled: false
				            },
				            exporting: {
				                enabled: false
				            },
				            series: [{
				                name: 'CPU使用率',
				                data: (function() {
				                    // generate an array of random data
				                    var data = [],
				                        time = (new Date()).getTime(),
				                        i;
				                    for (i = -59; i <= 0; i++) {
				                        data.push({
				                            x: time + i * 1000,
				                            y: Math.random()*100
				                        });
				                    }
				                    return data;
				                })()
				        	}]
			        });
			    });
			});
		</script>
	</head>
	<body>
	<div id="container" style="height: 300px; margin: 0 auto"></div>
</body>
</html>
