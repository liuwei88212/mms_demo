<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<form id="pagerForm" method="post" action="w_list.html">
	<input type="hidden" name="pageNum" value="1" /> <input type="hidden"
	name="numPerPage" value="${model.numPerPage}" /> <input type="hidden"
	name="orderField" value="${param.orderField}" /> <input type="hidden"
	name="orderDirection" value="${param.orderDirection}" />
</form>
<style>
	.disk .chartDiv {
		width: 32%;
		height: 97%;
		float: left;
		margin-right: 10px;
	}
	.disk .chartDiv h1{
		text-align: center;
	}
</style>
<script type="text/javascript">
	function dbltable(target,rel){		
		if(target=="disk_id"){
			$.pdialog.open("<%=request.getContextPath() %>/disk/detail.do", "disk_detail_dialog",'查看磁盘详情',
					{width:800,height:450,mask:true,fresh:true,resizable:false,mixable:false,minable:true});
		}
	}
</script>
<div class="pageHeader">
	<form rel="pagerForm" onsubmit="return navTabSearch(this);"
		action="demo_page1.html" method="post">
		<div class="searchBar disk " layoutH="347">
			<div class="chartDiv" style="">	
				<iframe name="disk_list_frame1" marginwidth="0" marginheight="0"
					width="100%" height="94%"
					src="<%=request.getContextPath()%>/chart/disk-list-pie.jsp?id=1"
					scrolling="auto" frameborder=0></iframe>
				<h1>盘符：/dev/sda1</h1>
			</div>
			
			<div class="chartDiv" style="">
				<iframe name="disk_list_frame2" marginwidth="0" marginheight="0"
					width="100%" height="95%"
					src="<%=request.getContextPath()%>/chart/disk-list-pie.jsp?id=2"
					scrolling="auto" frameborder=0></iframe>
				<h1>盘符：/dev/sda2</h1>
			</div>
			
			<div class="chartDiv" style="">	
				<iframe name="disk_list_frame1" marginwidth="0" marginheight="0"
					width="100%" height="95%"
					src="<%=request.getContextPath()%>/chart/disk-list-pie.jsp?id=1"
					scrolling="auto" frameborder=0></iframe>
				<h1>盘符：/dev/sda3</h1>
			</div>
		</div>
	</form>
</div>

<div class="pageContent">
	<table class="list" width="100%" targetType="navTab"  nowrapTD="false"  layoutH="295">
		<thead>
			<tr>
				<th>盘符</th>
				<th>文件类型</th>
				<th>总大小</th>
				<th>剩余空间</th>
				<th>可用空间</th>
				<th>已使用</th>
				<th>使用率</th>
				<th>磁盘读次数</th>
				<th>磁盘写次数</th>
			</tr>
		</thead>
		<tbody>
			<tr target="disk_id" rel="1">
				<td>/dev/sda3</td>
				<td>ext3</td>
				<td>906.8 G</td>
				<td>733.5 G</td>
				<td>687.4 G</td>
				<td>173.3 G</td>
				<td>21.0 %</td>
				<td>2,103,864 次</td>
				<td>42,836,539 次</td>
			</tr>
			<tr target="disk_id" rel="1">
				<td>/dev/sda3</td>
				<td>ext3</td>
				<td>906.8 G</td>
				<td>733.5 G</td>
				<td>687.4 G</td>
				<td>173.3 G</td>
				<td>21.0 %</td>
				<td>2,103,864 次</td>
				<td>42,836,539 次</td>
			</tr>
			<tr target="disk_id" rel="1">
				<td>/dev/sda3</td>
				<td>ext3</td>
				<td>906.8 G</td>
				<td>733.5 G</td>
				<td>687.4 G</td>
				<td>173.3 G</td>
				<td>21.0 %</td>
				<td>2,103,864 次</td>
				<td>42,836,539 次</td>
			</tr>
		</tbody>
	</table>
	<div class="panelBar">
		<div class="pages">
			<span>显示</span> <select name="numPerPage"
				onchange="navTabPageBreak({numPerPage:this.value})">
				<option value="20">100</option>
				<option value="50">200</option>
				<option value="100">300</option>
				<option value="200">全部</option>
			</select> <span>条，共3条</span>
		</div>
		<div class="pagination" targetType="navTab" totalCount="1"
			numPerPage="100" pageNumShown="1" currentPage="1"></div>
	</div>
</div>