<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<form id="pagerForm" method="post" action="###">
	<input type="hidden" name="pageNum" value="1" />
	<input type="hidden" name="numPerPage" value="${model.numPerPage}" />
	<input type="hidden" name="orderField" value="${param.orderField}" />
	<input type="hidden" name="orderDirection" value="${param.orderDirection}" />
</form>
<style>
	.cpu .iframeDiv{
		width: 32.5%;
		height:100%;
		float: left;
		margin-right: 5px;
	}
</style>
<script type="text/javascript">
	function dbltable(target,rel){		
		if(target=="cpu_id"){
			$.pdialog.open("<%=request.getContextPath() %>/cpu/detail.do", "cpu_detail_dialog",'查看CPU详情',
					{width:800,height:450,mask:true,fresh:true,resizable:true,mixable:false,minable:true});
		}
	}
	$(function(){
		//$(".pageContent").layoutH();	
	})
</script>
<div class="pageHeader" style="border:1px solid">
	<form rel="pagerForm" onsubmit="return navTabSearch(this);" action="###" method="post">
		<div class="cpu">
			<div style="" class="iframeDiv">
				<iframe name="cpu_list_frame1" marginwidth="0" marginheight="0" width="100%" height="100%"
					src="<%=request.getContextPath() %>/chart/cpu-list-line.jsp?id=1" scrolling="auto" frameborder=0></iframe>
			</div>
			<div style="" class="iframeDiv">
				<iframe name="cpu_list_frame2" marginwidth="0" marginheight="0" width="100%" height="100%"
					src="<%=request.getContextPath() %>/chart/cpu-list-line.jsp?id=2" scrolling="auto" frameborder=0></iframe>
			</div>
			<div style="" class="iframeDiv">
				<iframe name="cpu_list_frame3" marginwidth="0" marginheight="0" width="100%" height="100%"
					src="<%=request.getContextPath() %>/chart/cpu-list-line.jsp?id=3" scrolling="auto" frameborder=0></iframe>
			</div>
		</div>
	</form>
</div>

<div class="pageContent" style="border:1px solid;">
	<table class="table" width="99%" targetType="navTab" asc="asc" desc="desc" layoutH="235">
		<thead>
			<tr>
				<th>进程号</th>
				<th>进程名称</th>
				<th>状态</th>
				<th>CPU使用率</th>
				<th>内存使用率</th>
				<th>响应时间</th>
				<th>用户</th>
			</tr>
		</thead>
		<tbody>
			<tr target="cpu_id" rel="1">
				<td>1000</td>
				<td>360安全浏览器</td>
				<td>正在运行</td>
				<td>2%</td>
				<td>1%</td>
				<td>10ms</td>
				<td>Administrator</td>
			</tr>
			<tr target="cpu_id" rel="2">
				<td>1000</td>
				<td>360安全浏览器</td>
				<td>正在运行</td>
				<td>2%</td>
				<td>1%</td>
				<td>10ms</td>
				<td>Administrator</td>
			</tr>
			<tr target="cpu_id" rel="3">
				<td>1000</td>
				<td>360安全浏览器</td>
				<td>正在运行</td>
				<td>2%</td>
				<td>1%</td>
				<td>10ms</td>
				<td>Administrator</td>
			</tr>
		</tbody>
	</table>
	<div class="panelBar" >
		<div class="pages">
			<span>显示</span>
			<select name="numPerPage" onchange="navTabPageBreak({numPerPage:this.value})">
				<option value="20">20</option>
				<option value="50">50</option>
				<option value="100">100</option>
				<option value="200">200</option>
			</select>
			<span>条，共23条</span>
		</div>
		<div class="pagination" targetType="navTab" totalCount="200" numPerPage="20" pageNumShown="10" currentPage="2"></div>
	</div>
</div>